# Java-Retail-App

## Installation
Clone the repo using: 
```
git clone https://bitbucket.org/euriskoteam/retail-website-java-backend-marc.git
cd java-spring-app
```

## Usage
First, make sure you installed `maven` globally on your machine.
After that, open the cloned repository in Eclipse or any Java IDE of your choice, then build the project to get all the dependencies, by simply running:
```
mvn install
```

## Code Testing
To test the code, right-click on the project from the IDE and run the JUnit Test.
Or just open the terminal in the root directory, then run:

```
mvn test
```

## UML Diagram
![uml](https://user-images.githubusercontent.com/31523264/53755896-61628400-3ec0-11e9-9bcf-9811e75463c3.PNG)

## Code Coverage Results
![code_coverage](https://user-images.githubusercontent.com/31523264/53756176-37f62800-3ec1-11e9-90a6-383097f42b46.PNG)

