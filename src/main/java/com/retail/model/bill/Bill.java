package com.retail.model.bill;

import java.util.ArrayList;
import java.util.UUID;

import com.retail.model.user.User;
import com.retail.model.item.Item;

public class Bill {
    private ArrayList<Item> items;
    private String id;
    private User user;

    /**
     * @param items
     * @param user
     */
    public Bill(ArrayList<Item> items, User user) {
        this.id = UUID.randomUUID().toString();
        this.items = items;
        this.user = user;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * This method calculates the percentage that should be reduced from the bill
     * @param price
     * @param bill
     * @return double
     */
    public static Double percentageDiscount(Double price, Bill bill) {
        switch(bill.getUser().getType()) {
            case CUSTOMER:
                price -= User.checkOldCustomer(bill.getUser().getCreatedAt(), price);
                break;
            case EMPLOYEE:
                price -= price * 0.3;
                break;
            case AFFILIATE:
                price -= price *0.1;
                break;
        }
        return price;
    }

    /**
     * this method returns the amount with the 5% discount on each 100$ spent on the bill
     * @param total
     * @return double
     */
    public static double calculateAmount(double total) {
        return (int) (total / 100) * 5;
    }

}
