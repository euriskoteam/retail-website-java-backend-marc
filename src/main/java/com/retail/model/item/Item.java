package com.retail.model.item;

import com.retail.model.item.ItemCat;

import java.util.UUID;

public class Item {
    private String id;
    private String name;
    private ItemCat type;
    private Double price;

    /**
     * @param name
     * @param type
     * @param price
     */
    public Item(String name, ItemCat type, Double price) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ItemCat getType() {
        return type;
    }

    public void setType(ItemCat type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
