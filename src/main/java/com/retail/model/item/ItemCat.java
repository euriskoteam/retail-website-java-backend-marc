package com.retail.model.item;

public enum ItemCat {
        GROCERIES, ELECTRONICS, CLOTHES, FURNITURE
}
