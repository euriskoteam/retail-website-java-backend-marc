package com.retail.model.user;

public enum UserType {
    CUSTOMER, EMPLOYEE, AFFILIATE
}
