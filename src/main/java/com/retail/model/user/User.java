package com.retail.model.user;

import com.retail.utils.YearUtil;

import java.util.Date;
import java.util.UUID;

public class User {
    private String id;
    private String name;
    private UserType type;
    private Date createdAt;

    /**
     * @param name
     * @param type
     * @param createdAt
     */
    public User(String name, UserType type, Date createdAt) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.type = type;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * This method is used to check if the customer is old, if so, he will get 5% discount on his bill
     * @param createdAt
     * @param price
     * @return double
     */
    public static Double checkOldCustomer(Date createdAt, Double price) {
        int yearDiff = YearUtil.yearDifference(createdAt);
        if(yearDiff >= 2) {
            price = price * 0.05;
            return price;
        } else {
            return 0.0;
        }
    }
}

